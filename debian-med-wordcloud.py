#!/usr/bin/python3

import popcon
import apt

import wordcloud
import matplotlib.pyplot as plt

med_tasks = [
    'bio',
    'bio-dev',
    'cloud',
    'data',
    'epi',
    'imaging',
    'imaging-dev',
    'laboratory',
    'pharmacy',
    'physics',
    'practice',
    'psychology',
    'research',
    'statistics',
    'tools',
    'typesetting',
    ]

exclude = {
            'gmic',
            'octave',
            'r-cran-foreign',
            'texlive-latex-extra',
            'texlive-science',
            'workrave',
          }

renaming = {
    'python-biopython': 'biopython',
    'tigr-glimmer' : 'glimmer',
}

multiplier = {
    'clustalo'          : 2,
    'clustalw'          : 0.5,
    'cwltool'           : 4,
    'dcmtk'             : 0.5,
    'fastqc'            : 2,
    'gdcm'              : 0.5,
    'jalview'           : 2,
    'qiime'             : 4,
    'r-bioc-genefilter' : 2,
    'r-cran-qtl'        : 2,
    'r-cran-vegan'      : 2,
    'xmedcon'           : 0.2,
}


packages = {}
c = apt.Cache()
for t in med_tasks:
    print(t)
    tt = {}
    packages[t] = tt
    debian_task = 'med-' + t
    if debian_task not in c:
        print("{} not in cache".format(debian_task))
        continue
    p = c.get(debian_task).versions[-1]
    for r in p.recommends:
        name = r[0].name
        if name not in c:
            continue
        pt = c[name].versions[-1]
        srcname = pt.source_name
        maint = pt.record['maintainer']
        if name not in exclude:
            # pp = popcon.package(name) # that's "install"
            pp = popcon.packages_raw([name,])
            srcname = renaming.get(srcname, srcname)
            tt.setdefault(srcname, 0)
#            tt[srcname] = max(pp[name], tt[srcname])
            tt[srcname] += pp[name].vote * multiplier.get(srcname, 1)

frequencies = {}
tasks = {} # later to be used for color
for t, p in packages.items():
    for pp in p:
        frequencies.setdefault(pp, 0)
        frequencies[pp] += p[pp]
        tasks[pp] = t

w = wordcloud.WordCloud(max_font_size=200,
                        background_color="#fff0",
                        width=860,
                        height=1900)
w = w.generate_from_frequencies(frequencies)
w.to_file("packagecloud.png")
#plt.figure()
#plt.imshow(w, interpolation="bilinear")
#plt.axis("off")
#plt.show()
